package com.abc.PizzaOnlineStore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaOnlineStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaOnlineStoreApplication.class, args);
	}

}
